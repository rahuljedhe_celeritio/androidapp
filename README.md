# Interns #

This Repository contain the small sample application created using java and android. those project are listed below :


[TOC]

interns 
> -Documents 
>
> -Encryption 
>
> -Android 
>
> > -BluetoothApp 
>
> > -CameraApp 
>
> > -GoogleMap 
>
> > -GraphApp 
>
> > -HeimdallApp 
>
> > -PhilJayGraphApp 
>
> > -SearchingApp 
>
> > -WifiConnectivityApp 
>
> -grpc-Cassendra(server)

## Encryption ##
* Encryption is an interface which contain method declearation. 
* AESEncryption and RSAEncryption are classes implemented interface Encryption. 
* Those two classes are having definition of overrided method. 
* AESEncryption and RSAEncryption class are define two mechanism of implementing Encryption and Decryption of data i.e. AES and RSA. 
* AES work with SecretKey and RSA with public and private key. 
* Data must send into byte format and keys into Object type

##Haimdall##

> ###Flash Screen###
> 
> * Check should be complete within given period of time.
> * After validation, timeout app will show login screen, with session expired message.
> + Check if app is already logged in by user.
>     * if not then goto login page
>     + if logged in by user then check session at server side
>         * if session started at same device then go to Dashboard
>         * if session started at different device then goto login page
> 
> ###Sqlite###
> * Select query to sqlite database to check user record is available in profile table or not.
> 
> ###GRPC###
> * Grpc client sends user credentials with device Id for validation
> * Grpc server validates user credentials and checks last device session
> * Grpc server sends response to grpc client
> 
> ###Cassandra###
> * Select User Credentials from database and return to grpc server
> 
> 
> 
> ###Login Screen###
> 
> * User Login using three modes
> * UserName and Password
> * Gmail Sign In
> * Facebook Sign In
> * validate the username and password (Validation: not blank, password length, etc)
> * �show password� checkbox, to make typed password visible.
> * sign up option available for un-registered user
> * forget password option, allows user to get password on registered mobile number or e-mail address
> * if user credential are matched then go to dashboard page
> * if user authentication failed then show error message
> * if authentication failed by gmail and fb Sign In then fetch user profile Details  from fb/Gmail Account and goto registration page
> 
> ###Sqlite###
> * After grpc authentication getUserDetails and insert into local database
> 
> ###GRPC###
> * Grpc client send the user credential for authentication
> + Grpc server validate user
>  * if credential are valid then send the userdetails to client
>  * if credential are not valid then send err msg
> 
> ###Cassandra###
> * fetch userCredential for validation from database
> * after validation fetch the userDetails
> 
> 
> ###Registration Screen###
> 
> * Auto Fill data if registration page is load from gmail / fb authentication failed
> * validate the registration details (e.g. no empty field, valid email and mobile, etc.)
> * user should accept terms and condition
> * After validation goto second step validation page for opt / captcha validation
> 
> ###Otp / Captcha Screen###
> 
> * User validate using captcha and otp generation
> * Otp is 6 numerical digit which is send via sms service
> * if sms received on same device then auto detect and filled the Otp code 
> * if sms not received on same device then user can type the Otp code manually
> * Captcha are case sensitive 4 letter word without blankspace and special characters
> * After Validation register user on server
> * if user is registered on server then goto dashboard page
> * if not registered the show err message
> 
> ###Sqlite###
> * After server confirmation insert the user details into local database
> 
> ###GRPC###
> * Grpc client send registration details to server
> * Grpc client check the userName availability and register user and send notification
> * if user not register send err message
> 
> ###Cassandra###
> * Select user name from userCredential (Table)
> * If user does not already exist, then add user to the database.
> 
> 
> ###Setting Screen###
> 
> + Setting screen will contain:
>  * Edit Profile
> * Connect with fb / gmail
> * Logout
> 
> ###Edit Profile###
> * Set Profile Picture from gallery or capture from camera.
> * Edit Profile Details( Name, Email, Mobile no,etc)
> * Profile will only be updated if there is change in Profile detail fields.
> * After successful updation,  go to setting screen.
> 
> ###SQLite###
> * Update profile details (local database is updated after server database is changed).
>
> 
> ###GRPC###
> * GRPC Client will send only updated user details to the GRPC server.
> + GRPC Server will validate all user credentials and update User Profile.
>  * If profile is successfully updated then notify GRPC client, if not send err message
> 
> ###Cassandra###
> * will updates main database. 
> 
> 
> ###Connect with fb / gmail###
> * Show dialog to log-in via facebook/gmail account.
> * After successful log-in of facebook/gmail account, add account to user credentials.
> * Show message received by server and redirect to the Settings screen.
> 
> 
> ###GRPC###
> * After 
> * GRPC Client will send facebook/gmail id, device id and username to the GRPC server.
> * GRPC Server will add new credentials to user account.
> * If profile is successfully added then notify GRPC client, if not send error message
> 
> ###Cassandra###
> * will add credentials to main database. 
> 
> ###SQLite###
> * Add facebook/gmail id to profile details (local database is updated after server database is changed).
> 
> 
> ###LogOut###
> * Delete User sessions from server and redirect to Log-in page.
> 
> ###SQLite###
> * Delete User Profile from local database.
> 
> ###GRPC###
> * GRPC Client will request GRPC Server to expire current device id (session).
> * GRPC Server will destroy the session and notify the GRPC client.
> 
> ###Cassandra###
> * will reset device id in main database. 


## Modules not yet Integrated ##
> 
> ### Backup ###
> + wifi
>  * Turn on Wifi-Direct.
>  * Scan for Peer Devices.
>  * Show Devices.
>  * Connect Device.
>  * List Files.
>  * Send Files.
>  * Disconnect.
> + bluetooth
>  * Turn on BlueTooth.
>  * Turn on BlueTooth Visibility.
>  * Pair Device.
>  * Scan for Paired Devices.
>  * Show Devices.
>  * Connect Device.
>  * Send File.
>  * Turn Off bluetooth.
> 
> ### Restore ###
> + wifi
>  * Turn on Wifi-Direct.
>  * Scan for Peer Devices.
>  * Show Devices.
>  * Connect Device.
>  * Receive File.
>  * Disconnect.
>  * Enable restore Option
>  * Show restore progress.
>  * Restart app.
>   
> + bluetooth
>  * Turn on BlueTooth.
>  * Turn on BlueTooth Visibility.
>  * Pair Device.
>  * Scan for Paired Devices.
>  * Show Devices.
>  * Connect Device.
>  * Receive file.
>  * Turn Off bluetooth.
>  * Enable Restore Button.
>  * Show restore progress.
>  * Restart app.
> 
> 
>  ### Google Map Positioning and navigation Module ###
>  
>  ### Dynamic Graph and chart generation ###
>
> ### Relative search (with suggestion) ###


Backlog

Online sms generation issue -
Buy sms services

Implement payment gateway issue -
Buy services for Merchant account (Payment API)

Chat and Voice Features
Heavy usage of Google API’s and large number of users will require purchasing the API and services.

    
Future Enhancement -
        Create Logger for App/Grpc server and client / cassandra
        Add exception handler for avoid app crashing
        App Forward/Backward Compatibility
        Full connection State management(Disconnect/Reconnect) for Client,Server and Database.
        ssl Security and Data Encryption.