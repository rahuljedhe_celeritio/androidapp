package com.example.kadirsk.bluetooth_example;

import android.os.FileObserver;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

public class PathFileObserver extends FileObserver {
    static final String TAG="FILEOBSERVER";
    /**
     * should be end with File.separator
     */
    String rootPath;


    public PathFileObserver(String root){
        super(root, FileObserver.CREATE );

        if (! root.endsWith(File.separator)){
            root += File.separator;
        }
        rootPath = root;
    }

    public void onEvent(int event, String path) {

        switch(event){
            case FileObserver.CREATE:
                Log.d(TAG, "CREATE:" + rootPath + path);
               // Toast.makeText(getApplicationContext(), "Turned off" ,Toast.LENGTH_LONG).show();
                break;

        }
    }

    public void close(){
        super.finalize();
    }
}