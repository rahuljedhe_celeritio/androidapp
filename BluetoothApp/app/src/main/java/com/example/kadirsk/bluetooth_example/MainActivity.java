package com.example.kadirsk.bluetooth_example;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.View;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MainActivity extends Activity  {
    Button b1,b2,b3,b4;
    String deviceAddress;
    private BluetoothAdapter BA;
    private Set<BluetoothDevice>pairedDevices;
    ListView lv;
    Button scanButton ;
    ListView scanListView;
    ArrayList<String> stringArrayList =new ArrayList<String>();
    ArrayAdapter<String> arrayAdapter;
    BluetoothAdapter myAdapter = BluetoothAdapter.getDefaultAdapter();
    private static final int DISCOVER_DURATION=300;
    private static final int REQUEST_BLU=1;


    public void myscan(View view)
    {
    /*    myAdapter.startDiscovery();
        Toast.makeText(getApplicationContext(), "Showing Paired Devices",Toast.LENGTH_SHORT).show();
        Log.v("","HELOOOO");

        BroadcastReceiver myReciver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Toast.makeText(getApplicationContext(), "In function:" , Toast.LENGTH_LONG).show();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    stringArrayList.add(device.getName());
                    arrayAdapter.notifyDataSetChanged();
                }
                Toast.makeText(getApplicationContext(), "Showing Paired Devices",Toast.LENGTH_SHORT).show();
            }
        };
        IntentFilter intentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(myReciver,intentFilter);

        arrayAdapter= new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1,stringArrayList);
        scanListView.setAdapter(arrayAdapter);

      *//*  Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image***//**//*");*//*
        //*//*intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File("soaring-bg.jpg")));
       // startActivity(intent);*/
















        ArrayList <BluetoothAdapter> arrayOfFoundBTDevices = new ArrayList<BluetoothAdapter>();

        // start looking for bluetooth devices
        myAdapter.startDiscovery();

        // Discover new devices
        // Create a BroadcastReceiver for ACTION_FOUND
        final BroadcastReceiver mReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String action = intent.getAction();
                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND.equals(action))
                {
                    // Get the bluetoothDevice object from the Intent
                    Toast.makeText(getApplicationContext(), "In function:" , Toast.LENGTH_LONG).show();

                    // Get the "RSSI" to get the signal strength as integer,
                    // but should be displayed in "dBm" units
                    int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,Short.MIN_VALUE);

                    // Create the device object and add it to the arrayList of devices
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    stringArrayList.add(device.getName());
                    arrayAdapter.notifyDataSetChanged();

                   // arrayOfFoundBTDevices.add(bluetoothObject);

                    // 1. Pass context and data to the custom adapter

                }
            }
        };
        // Register the BroadcastReceiver
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        b1 = (Button) findViewById(R.id.button);
        b2=(Button)findViewById(R.id.button2);
        b3=(Button)findViewById(R.id.button3);
        b4=(Button)findViewById(R.id.button4);

        BA = BluetoothAdapter.getDefaultAdapter();

       // scanListView=(ListView)findViewById(R.id.avaliable_device);
    }


    public  void send()
    {
        ContentValues values = new ContentValues();
        String filePath="/storage/emulated/0/DCIM/Camera/20171023_094532.jpg";
        values.put(BluetoothShare.URI, "content:" + filePath);
        values.put(BluetoothShare.DESTINATION, deviceAddress);
        Toast.makeText(getApplicationContext(), ""+deviceAddress ,Toast.LENGTH_SHORT).show();
        values.put(BluetoothShare.DIRECTION, BluetoothShare.DIRECTION_OUTBOUND);
        Long ts = System.currentTimeMillis();
        values.put(BluetoothShare.TIMESTAMP, ts);
        File file = new File(filePath);
        if(file.exists())
            Toast.makeText(getApplicationContext(), "File is present" ,Toast.LENGTH_SHORT).show();

       // getContentResolver().update(BluetoothShare.CONTENT_URI, values,null,null);

    }

    public void on(View v){
        if (!BA.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 0);
            Toast.makeText(getApplicationContext(), "Turned on",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Already on", Toast.LENGTH_LONG).show();
        }
    }

    public void off(View v){
        BA.disable();
        Toast.makeText(getApplicationContext(), "Turned off" ,Toast.LENGTH_LONG).show();
    }


    public  void visible(View v){
        Intent getVisible = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        startActivityForResult(getVisible, 0);
    }


    public void list(View v){
        pairedDevices = BA.getBondedDevices();
        ArrayList <String> list = new ArrayList <String>();
        for(BluetoothDevice bt : pairedDevices)
        {
            list.add(bt.getName());
            list.add(bt.getAddress());
            deviceAddress=bt.getAddress();
        }
        String path = getFilesDir().getAbsolutePath();
        Toast.makeText(getApplicationContext(), "Showing Paired Devices",Toast.LENGTH_SHORT).show();
        list.add(path);
        final ArrayAdapter <String> adapter = new  ArrayAdapter <>(this,android.R.layout.simple_list_item_1, list);
        lv.setAdapter(adapter);
        send();
        Toast.makeText(getApplicationContext(), "After send function",Toast.LENGTH_SHORT).show();
    }


    public void sendViaBluetooth(View view) {

        BluetoothAdapter btApadter = BluetoothAdapter.getDefaultAdapter();
        if(btApadter == null){
            Toast.makeText(this,"Bluetooth is not supppoted in this device",Toast.LENGTH_LONG).show();
        }
        else {
            enableBluetooth();
        }
    }

    public void enableBluetooth()
    {
        Intent discoveryIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);

        discoveryIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,DISCOVER_DURATION);

        startActivityForResult(discoveryIntent,REQUEST_BLU);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == DISCOVER_DURATION && requestCode == REQUEST_BLU)
        {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setType("*/*");
            String filePath="/storage/emulated/0/DCIM/Camera/20171023_094532.jpg";
            //File f = new File(Environment.getExternalStorageDirectory(),"Readme.txt");
            File file = new File(filePath);
            intent.putExtra(Intent.EXTRA_STREAM,Uri.fromFile(file));

            PackageManager pm = getPackageManager();
            List<ResolveInfo> appList = pm.queryIntentActivities(intent,0);

            if (appList.size() > 0)
            {
                String packageName = null;
                String className = null;
                boolean found = false;

            for (ResolveInfo info: appList)
            {
                packageName = info.activityInfo.packageName;
                if (packageName.equals("com.android.bluetooth"))
                {
                    className = info.activityInfo.name;
                    found=true;
                    break;
                }
            }
            if (!found)
                Toast.makeText(this,"Bluetooth Have't found",Toast.LENGTH_LONG).show();
            else
                {
                    intent.setClassName(packageName,className);
                    startActivity(intent);
                }
            }
        }
        else
            {
                Toast.makeText(this,"Bluetooth is cancelled...",Toast.LENGTH_LONG).show();
            }
    }

    public void myRecive(View view) {
        Intent intent= new Intent(MainActivity.this,ReciveBluetooth.class);
        startActivity(intent);
    }
}