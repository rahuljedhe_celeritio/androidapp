package com.example.kadirsk.bluetooth_example;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.FileObserver;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

public class ReciveBluetooth extends AppCompatActivity {
    private static final int DISCOVER_DURATION=300;
    private static final int REQUEST_BLU=1;
    private BluetoothAdapter BA;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recive_bluetooth);
        enableBluetooth();
       // visible();
       /* PathFileObserver pathFileObserver = new PathFileObserver("/sdcard/Bluetooth/");
        pathFileObserver.onEvent(FileObserver.CREATE ,"/sdcard/Bluetooth/");
*/

        try {

        //Code to move file from bluetooth to anothe directory.
        File file = new File("/sdcard/Download/abc.txt");
        file.renameTo(new File("/sdcard/data/Readme.txt"));


        final String pathToWatch ="/sdcard/Bluetooth/";

        Toast.makeText(this, "My Service Started and trying to watch " + pathToWatch, Toast.LENGTH_LONG).show();
        Log.d("Here am I:", "Inside create.....");
        FileObserver myobserver = new FileObserver(pathToWatch) { // set up a file observer to watch this directory on sd card
            @Override
            public void onEvent(int event, String file) {
                //if(event == FileObserver.CREATE && !file.equals(".probe")){ // check if its a "create" and not equal to .probe because thats created every time camera is launched
                Log.d(pathToWatch, "File created [" + pathToWatch + file + "]");
                Toast.makeText(getBaseContext(), file + " was saved!", Toast.LENGTH_LONG).show();
                //}
            }
        };
        myobserver.startWatching(); //START OBSERVING


        FileObserver observer = new FileObserver(file.getPath()) {
            public String basePath;

            @Override
            public void onEvent(int event, String path) {
                String fullPath = basePath;
                if(path != null) {
                    fullPath += path;
                }
                Toast.makeText(ReciveBluetooth.this, "My Service Started and trying to watch " + pathToWatch, Toast.LENGTH_LONG).show();
                Log.d("Here am I:", "On event....");
            }
        };
        //observer = file.getPath();
        observer.startWatching();

}
    catch (Exception e )
    {
        Log.d("Status", "Move file successful."+e);
        Toast.makeText(this, "Status "+e, Toast.LENGTH_LONG).show();
    }
    }

    public void enableBluetooth()
    {

        Intent discoveryIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);

        discoveryIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,DISCOVER_DURATION);

        startActivityForResult(discoveryIntent,REQUEST_BLU);
    }
    public void off(){
        BA.disable();
        Toast.makeText(getApplicationContext(), "Turned off" ,Toast.LENGTH_LONG).show();
    }
}
