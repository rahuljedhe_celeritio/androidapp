package abc;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

public class DatabaseC {
	   static Cluster  cluster=Cluster.builder().addContactPoint("127.0.0.1").build();
	   static Session session = cluster.connect("test");	
	 
	public static String getRecords(String table_name)
	{
		try {
		String query = "select * from "+table_name;
		ResultSet result = session.execute(query);
		return result.all().toString();
	}
	catch (Exception e) {return "Unable to fetch the data...";}
		
	}
	
	public static String getRecords(String table_name,int limit)
	{
		try {
		String query = "select * from "+table_name +" LIMIT "+limit;
		ResultSet result = session.execute(query);
		return result.all().toString();
	}
	catch (Exception e) {return "Unable to fetch the data...";}
		
	}
	public static String getRecords(String table_name,String cond)
	{
		try {
			String query = "select "+cond+" from "+table_name;
			ResultSet result;
			result = session.execute(query);
			return result.all().toString();			
		}
		catch (Exception e) {return "Unable to fetch the data...";}
		 

	}
	
	public static String insertProfile(String uname,String name,String address,int mobile,String gender,String dob)
	{
		try {
		String query = "INSERT INTO Profile (uname, name, address,mobile, gender,dob) VALUES('"+uname+"','"+name+"','"+address+"', "+mobile+", '"+gender+"','"+dob+"')";
		//System.out.println(query);
		ResultSet result = session.execute(query);
		return result.all().toString();
	}
	catch (Exception e) {return "Unable to insert the data...";}
	}
	

	public static String insertCredential(String uname,String password,String gmailId,String fb_id,String user_role)
	{
		try {
		String query = "INSERT INTO Credential(uname,password,gmailId,fb_id,user_role) VALUES('"+uname+"','"+password+"','"+gmailId+"', '"+fb_id+"','"+user_role+"')";
		ResultSet result = session.execute(query);
		return result.all().toString();
	}
	catch (Exception e) {return "Unable to insert the data...";}
	}
	
   public static void main(String args[]){
	   
	   System.out.println(getRecords("profile","uname"));
      //Query
      //String query = "select * from profile";

      //Creating Cluster object
     //Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
   
      //Creating Session object
      //Session session = cluster.connect("test");
 
      //Executing the query
    //  session.execute(query);
     // ResultSet result = session.execute(query);
      
 
     // System.out.println(result.all());
   }
}