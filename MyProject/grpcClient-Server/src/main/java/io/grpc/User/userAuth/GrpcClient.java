/*
 * Copyright 2015, gRPC Authors All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.grpc.User.userAuth;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * A simple client that requests a greeting from the {@link GrpcServer}.
 */
public class GrpcClient {
  private static final Logger logger = Logger.getLogger(GrpcClient.class.getName());

  private final ManagedChannel channel;
  private final AuthenticateUserGrpc.AuthenticateUserBlockingStub blockingStub;

  /** Construct client connecting to HelloWorld server at {@code host:port}. */
  public GrpcClient(String host, int port) {
    this(ManagedChannelBuilder.forAddress(host, port)
        // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
        // needing certificates.
        .usePlaintext(true)
        .build());
  }

  /** Construct client for accessing RouteGuide server using the existing channel. */
  GrpcClient(ManagedChannel channel) {
    this.channel = channel;
    blockingStub = AuthenticateUserGrpc.newBlockingStub(channel);
  }

  public void shutdown() throws InterruptedException {
    channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
  }

  /** Say hello to server. */
  public void getUserAuthenticated(String username, String password) 
  {
    logger.info("Verifying Credentials of " + username + " with server...");
    UserCredentials request = UserCredentials.newBuilder()
                                        .setUsername(username)
                                        .setPassword(password).build();
    ResultValue response;
    try 
    {
      response = blockingStub.verify(request);
    } 
    catch (StatusRuntimeException e) 
    {
      logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
      return;
    }
    String resultfromServer = response.getValue();
    logger.info("Result From Server : " + resultfromServer);
  }

  /**
   * Greet server. If provided, the first element of {@code args} is the name to use in the
   * greeting.
   */
  public static void main(String[] args) throws Exception
   {
    GrpcClient client = new GrpcClient("192.168.1.84", 50051);
    try
    {
      /* Access a service running on the local machine on port 50051 */
      Scanner sc = new Scanner(System.in);
      System.out.println("Enter username : ");
      String uname = sc.next();
      System.out.println("Enter password : ");
      String pswd = sc.next();
      
      client.getUserAuthenticated(uname,pswd);
    } 
    finally 
    {
      client.shutdown();
    }
  }
}
