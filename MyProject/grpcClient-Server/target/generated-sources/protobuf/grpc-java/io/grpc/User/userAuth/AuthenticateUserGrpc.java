package io.grpc.User.userAuth;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 * <pre>
 * The greeting service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.7.0)",
    comments = "Source: UserAuth.proto")
public final class AuthenticateUserGrpc {

  private AuthenticateUserGrpc() {}

  public static final String SERVICE_NAME = "userAuth.AuthenticateUser";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<io.grpc.User.userAuth.UserCredentials,
      io.grpc.User.userAuth.ResultValue> METHOD_VERIFY =
      io.grpc.MethodDescriptor.<io.grpc.User.userAuth.UserCredentials, io.grpc.User.userAuth.ResultValue>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "userAuth.AuthenticateUser", "verify"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              io.grpc.User.userAuth.UserCredentials.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              io.grpc.User.userAuth.ResultValue.getDefaultInstance()))
          .setSchemaDescriptor(new AuthenticateUserMethodDescriptorSupplier("verify"))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static AuthenticateUserStub newStub(io.grpc.Channel channel) {
    return new AuthenticateUserStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static AuthenticateUserBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new AuthenticateUserBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static AuthenticateUserFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new AuthenticateUserFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static abstract class AuthenticateUserImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void verify(io.grpc.User.userAuth.UserCredentials request,
        io.grpc.stub.StreamObserver<io.grpc.User.userAuth.ResultValue> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_VERIFY, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_VERIFY,
            asyncUnaryCall(
              new MethodHandlers<
                io.grpc.User.userAuth.UserCredentials,
                io.grpc.User.userAuth.ResultValue>(
                  this, METHODID_VERIFY)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class AuthenticateUserStub extends io.grpc.stub.AbstractStub<AuthenticateUserStub> {
    private AuthenticateUserStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AuthenticateUserStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AuthenticateUserStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AuthenticateUserStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void verify(io.grpc.User.userAuth.UserCredentials request,
        io.grpc.stub.StreamObserver<io.grpc.User.userAuth.ResultValue> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_VERIFY, getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class AuthenticateUserBlockingStub extends io.grpc.stub.AbstractStub<AuthenticateUserBlockingStub> {
    private AuthenticateUserBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AuthenticateUserBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AuthenticateUserBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AuthenticateUserBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public io.grpc.User.userAuth.ResultValue verify(io.grpc.User.userAuth.UserCredentials request) {
      return blockingUnaryCall(
          getChannel(), METHOD_VERIFY, getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class AuthenticateUserFutureStub extends io.grpc.stub.AbstractStub<AuthenticateUserFutureStub> {
    private AuthenticateUserFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private AuthenticateUserFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected AuthenticateUserFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new AuthenticateUserFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<io.grpc.User.userAuth.ResultValue> verify(
        io.grpc.User.userAuth.UserCredentials request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_VERIFY, getCallOptions()), request);
    }
  }

  private static final int METHODID_VERIFY = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final AuthenticateUserImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(AuthenticateUserImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_VERIFY:
          serviceImpl.verify((io.grpc.User.userAuth.UserCredentials) request,
              (io.grpc.stub.StreamObserver<io.grpc.User.userAuth.ResultValue>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class AuthenticateUserBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    AuthenticateUserBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return io.grpc.User.userAuth.UserAuthProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("AuthenticateUser");
    }
  }

  private static final class AuthenticateUserFileDescriptorSupplier
      extends AuthenticateUserBaseDescriptorSupplier {
    AuthenticateUserFileDescriptorSupplier() {}
  }

  private static final class AuthenticateUserMethodDescriptorSupplier
      extends AuthenticateUserBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    AuthenticateUserMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (AuthenticateUserGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new AuthenticateUserFileDescriptorSupplier())
              .addMethod(METHOD_VERIFY)
              .build();
        }
      }
    }
    return result;
  }
}
