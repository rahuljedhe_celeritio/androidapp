package com.example.celeritio.myproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForgetPassword extends AppCompatActivity {

    private static EditText emailId;
    private static TextView submit, back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        emailId = (EditText) findViewById(R.id.registered_emailid);
        submit = (TextView) findViewById(R.id.forgot_button);
        back = (TextView) findViewById(R.id.backToLoginBtn);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getEmailId = emailId.getText().toString();

                // Pattern for email id validation
                Pattern p = Pattern.compile(Utils.regEx);

                // Match the pattern
                Matcher m = p.matcher(getEmailId);

                // First check if email id is not null else show error toast
                if (getEmailId.equals("") || getEmailId.length() == 0)
                    Toast.makeText(ForgetPassword.this,
                            "Please enter your Email Id.",Toast.LENGTH_LONG).show();
                    // Check if email id is valid or not
                else if (!m.find())
                    Toast.makeText(ForgetPassword.this,
                            "Your Email Id is Invalid.",Toast.LENGTH_LONG).show();

                    // Else submit email id and fetch passwod or do your stuff
                else
                    Toast.makeText(ForgetPassword.this,
                            "Get Forgot Password.",Toast.LENGTH_LONG).show();
            }
        });

    }
}
