package com.example.celeritio.myproject;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Arrays;
import java.util.List;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    EditText userName, password;
    GoogleApiClient googleApiClient;
    GoogleSignInOptions signInOptions;
    SignInButton gmail;
    Button login;
    LoginButton loginButton;
    CallbackManager callbackManager;
    CheckBox show_hide_password;
    TextView signUp;
    //static username password replace with your Crediential
    public static String[] crediential={"Rahul:Rahul@123","Teja:Teja@123"};
    public static String[] googleLogin={"rahulrkj1@gmail.com","Teja@gmail.com"};
    public static String[] fbLogin={"1957998824442100","1957998824442122"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        userName=(EditText)findViewById(R.id.login_emailid);
        password=(EditText)findViewById(R.id.login_password);
        login = (Button) findViewById(R.id.loginBtn);
        loginButton=(LoginButton)findViewById(R.id.facebook);
        signUp = (TextView) findViewById(R.id.createAccount);
        show_hide_password = (CheckBox) findViewById(R.id.show_hide_password);
        gmail = (SignInButton) findViewById(R.id.gmail);
        signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient=new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(Auth.GOOGLE_SIGN_IN_API,signInOptions).build();
        TextView forgetpass=(TextView) findViewById(R.id.forgot_password);
        callbackManager=CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logOut();
                Intent intent=new Intent(Login.this,Registration.class);
                startActivity(intent);
            }
        });
        show_hide_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {

                    show_hide_password.setText(R.string.hide_pwd);// change

                    password.setInputType(InputType.TYPE_CLASS_TEXT);
                    password.setTransformationMethod(HideReturnsTransformationMethod
                            .getInstance());// show password
                } else {
                    show_hide_password.setText(R.string.show_pwd);// change

                    password.setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    password.setTransformationMethod(PasswordTransformationMethod
                            .getInstance());// hide password

                }
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                defaultLogin();
            }
        });

        gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gmailLogin();
            }
        });

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                    String userid=loginResult.getAccessToken().getUserId();
                    if(Arrays.asList(fbLogin).contains(userid)) {
                        LoginManager.getInstance().logOut();
                        Intent intent=new Intent(Login.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        LoginManager.getInstance().logOut();
                        Intent intent=new Intent(Login.this,Registration.class);
                        startActivity(intent);
                    }

            }

            @Override
            public void onCancel() {
                Toast.makeText(Login.this,"Login Cancled",Toast.LENGTH_LONG);
            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        //forget password click event

        forgetpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgetPassword();
            }
        });

    }
    public void forgetPassword()
    {
        Intent intent = new Intent(Login.this,ForgetPassword.class);
        startActivity(intent);
    }


    public void gmailLogin()
    {
        Intent intent= Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent,9001);
    }
    public void defaultLogin()
    {
        for(String check:crediential)
        {
            String[] value=check.split(":");
            if(value[0].equals(userName.getText().toString().trim()))
            {
                if (value[1].equals(password.getText().toString().trim()))
                {
                    Toast.makeText(Login.this,"Welcome "+userName.getText(),Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Login.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                }
                else
                {
                    Toast.makeText(Login.this,"Wrong Password",Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(Login.this,"User Name is not Valid",Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(requestCode==9001)
        {
            GoogleSignInResult result=Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }
    }

    private void handleResult(GoogleSignInResult result) {
        if(result.isSuccess()){
            GoogleSignInAccount account = result.getSignInAccount();
            String email = account.getEmail();
            String name = account.getDisplayName();
            List mylist = Arrays.asList(googleLogin);
            if (mylist.contains(email)) {
                int i = mylist.indexOf(email);
                Intent intent=new Intent(Login.this,MainActivity.class);
                intent.putExtra("MailId",email);
                intent.putExtra("Name",name);
                startActivity(intent);
                finish();
            }
            else
            {
                Intent intent=new Intent(Login.this,Registration.class);
                intent.putExtra("MailId",email);
                intent.putExtra("Name",name);
                startActivity(intent);
            }
        }
        else
        {
            Toast.makeText(Login.this,"Login Failed",Toast.LENGTH_LONG).show();
        }
    }
}
