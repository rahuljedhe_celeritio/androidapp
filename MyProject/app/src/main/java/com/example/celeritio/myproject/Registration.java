package com.example.celeritio.myproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Registration extends AppCompatActivity {
    private static EditText fullName, emailId, mobileNumber, location,
            password, confirmPassword;
    private static TextView login;
    private static Button signUpButton;
    private static CheckBox terms_conditions;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        fullName = (EditText) findViewById(R.id.fullName);
        emailId = (EditText) findViewById(R.id.userEmailId);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        location = (EditText) findViewById(R.id.location);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        signUpButton = (Button) findViewById(R.id.signUpBtn);
        login = (TextView) findViewById(R.id.already_user);
        terms_conditions = (CheckBox) findViewById(R.id.terms_conditions);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String getFullName = fullName.getText().toString();
                String getEmailId = emailId.getText().toString();
                String getMobileNumber = mobileNumber.getText().toString();
                String getLocation = location.getText().toString();
                String getPassword = password.getText().toString();
                String getConfirmPassword = confirmPassword.getText().toString();

                // Pattern match for email id
                Pattern p = Pattern.compile(Utils.regEx);
                Matcher m = p.matcher(getEmailId);

                // Check if all strings are null or not
                if (getFullName.equals("") || getFullName.length() == 0
                        || getEmailId.equals("") || getEmailId.length() == 0
                        || getMobileNumber.equals("") || getMobileNumber.length() == 0
                        || getLocation.equals("") || getLocation.length() == 0
                        || getPassword.equals("") || getPassword.length() == 0
                        || getConfirmPassword.equals("")
                        || getConfirmPassword.length() == 0)

                    Toast.makeText(Registration.this,"All fields are required.",Toast.LENGTH_LONG).show();

                    // Check if email id valid or not
                else if (!m.find())
                    Toast.makeText(Registration.this,"Your Email Id is Invalid.",Toast.LENGTH_LONG).show();

                    // Check if both password should be equal
                else if (!getConfirmPassword.equals(getPassword))
                    Toast.makeText(Registration.this,"Both password doesn't match.",Toast.LENGTH_LONG).show();

                    // Make sure user should check Terms and Conditions checkbox
                else if (!terms_conditions.isChecked())
                    Toast.makeText(Registration.this,"Please select Terms and Conditions.",Toast.LENGTH_LONG).show();

                    // Else do signup
                else
                {
                    int res = OnReg();
                    if(res==1)
                    {
                        Toast.makeText(Registration.this, "Registered Sussfull...", Toast.LENGTH_SHORT)
                                .show();
                    }
                    else
                    {
                        Toast.makeText(Registration.this, "Unable to Register...", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            }
        });
    }
    public  int  OnReg()
    {
        String getFullName = fullName.getText().toString();
        String getEmailId = emailId.getText().toString();
        String getMobileNumber = mobileNumber.getText().toString();
        String getLocation = location.getText().toString();
        String getPassword = password.getText().toString();
        String getConfirmPassword = confirmPassword.getText().toString();
        String type = "register";
        //BackgroundWorker backgroundWorker = new BackgroundWorker();
        //int r = backgroundWorker.mybackground(type, getFullName, getEmailId, getPassword, getMobileNumber, getLocation);
        //return r;
        return 0;
    }
}
